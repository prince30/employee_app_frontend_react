import axios from 'axios';

const EMPLOYEE_API_BASE_URL = "http://localhost:8989/employees";

class EmployeeService {

    getAll(){
        return axios.get(EMPLOYEE_API_BASE_URL, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    create(data){
        return axios.post(EMPLOYEE_API_BASE_URL, data, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    getById(id){
        return axios.get(EMPLOYEE_API_BASE_URL + '/' + id, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    update(data, id){
        return axios.put(EMPLOYEE_API_BASE_URL + '/' + id, data, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    delete(id){
        return axios.delete(EMPLOYEE_API_BASE_URL + '/' + id, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }
	
	getByDepartmentId(departmentId){
        return axios.get(EMPLOYEE_API_BASE_URL + '/department/' + departmentId, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }
}

export default new EmployeeService()