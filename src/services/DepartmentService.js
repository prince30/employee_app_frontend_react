import axios from 'axios';

const DEPARTMENT_API_BASE_URL = "http://localhost:8989/departments";

class DepartmentService {

    getAll(){
        return axios.get(DEPARTMENT_API_BASE_URL, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    create(data){
        return axios.post(DEPARTMENT_API_BASE_URL, data, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    getById(id){
        return axios.get(DEPARTMENT_API_BASE_URL + '/' + id, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    update(data, id){
        return axios.put(DEPARTMENT_API_BASE_URL + '/' + id, data, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }

    delete(id){
        return axios.delete(DEPARTMENT_API_BASE_URL + '/' + id, {headers: {'Content-Type':'application/json; charset=UTF-8'}});
    }
}

export default new DepartmentService()