import React, { Component } from 'react';
import {Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';
import {Route, BrowserRouter as Router, Switch, Link} from 'react-router-dom';
import DepartmentList from  './DepartmentList';
import EmployeeList from  './EmployeeList';
import UpdateDepartment from  './UpdateDepartment';
import UpdateEmployee from  './UpdateEmployee';
import EmployeesByDepartment from  './EmployeesByDepartment';


class AppNavbar extends Component {
	
    render() {
        return (
		<Router>
			<div>
			  <Navbar color="dark" dark expand="md">
				<NavbarBrand className="nav-link" to="/">Employee management system application</NavbarBrand>
				<Nav className="mr-auto" navbar>
					<NavItem>
					  <Link exact className="nav-link" to="/department">Departments</Link>
					</NavItem>
					<NavItem>
					  <Link exact className="nav-link" to="/employee">Employees</Link>
					</NavItem>
				</Nav>	
			  </Navbar>
			  
					<Switch>
					<Route exact path="/" component={DepartmentList} />
					<Route exact path="/department" component={DepartmentList} />
					<Route exact path="/employee" component={EmployeeList} />
					<Route path = "/update-department/:departmentId" component = {UpdateDepartment}></Route>
					<Route path = "/update-employee/:employeeId" component = {UpdateEmployee}></Route>
					<Route path = "/departments/:departmentId/employees" component = {EmployeesByDepartment}></Route>
					</Switch>
			</div>
		</Router>	
		  );
    }
}

export default AppNavbar;