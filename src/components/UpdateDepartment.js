import React, { Component } from 'react'
import DepartmentService from '../services/DepartmentService'
import { Container, Col, Button, Form, FormGroup, Label, Input, Table } from 'reactstrap'
import '../custom.css'
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

toast.configure()

class DepartmentList extends Component {
    constructor(props) {
        super(props)

        this.state = {
				id: this.props.match.params.departmentId,
				name:"",
				active:false,
				errors: {}
        }
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleChecked=this.handleChecked.bind(this);
    }
	
    componentDidMount(){
		console.log(this.state.id);
        DepartmentService.getById(this.state.id).then((res) => {
            let department = res.data;
            this.setState({name: department.name,
                active: department.active
            });
        }).catch(error=> {
		  console.log(error);
		});
    }
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleChecked = () => {
		this.setState(initialState => ({
		  active: !initialState.active,
		}));
	}
	
	handleValidation = () => {
		let name = this.state.name;
		let errors = {};
		let formIsValid = true;

		if (!name) {
		  formIsValid = false;
		  errors["name"] = "Name Cannot be empty";
		}

		this.setState({ errors: errors });
		return formIsValid;
	  }
	  
	handleSubmit=(e)=>{
		e.preventDefault();

		if (this.handleValidation()) {
			var postData={
			name:this.state.name,
			active:this.state.active
			}

			DepartmentService.update(postData, this.state.id)
			.then(response => {
				toast("Department updated successfully",{autoClose:2000, position: toast.POSITION.TOP_CENTER})  
			})
			.catch(error=> {
			  console.log(error);
			})
		  
		} else {
		  
		}
	}


    render() {
		const title=<h3>Update Department</h3>

		
        return (
            <div>
                <Container>
					{title}
					<Form className="dept-form">
						<FormGroup row>
							<Label for="exampleEmail">Name</Label>
							<Col sm={4}>
								<Input type="text" name="name" className="form-control" id="name" value={this.state.name} onChange={this.handleChange} placeholder="" />
								<p style={{ color: "red" }}>{this.state.errors["name"]}</p>
							</Col>
							<Col sm={6} className="check-box">
								<Label check>
									<Input type="checkbox" name="active" id="active" checked={this.state.active} onChange={this.handleChecked} />
									Active
								</Label>
								<Button className="btn-sm btn-submit" onClick={this.handleSubmit}>Submit</Button>
							</Col>
						</FormGroup>
					</Form>
				</Container>
            </div>
        );
    }
}

export default DepartmentList