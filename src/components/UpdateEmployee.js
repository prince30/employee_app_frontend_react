import React, { Component } from 'react'
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import EmployeeService from '../services/EmployeeService'
import DepartmentService from '../services/DepartmentService'
import { Container, Col, Button, Form, FormGroup, Label, Input, Table } from 'reactstrap'
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import '../custom.css'

toast.configure()

class UpdateEmployee extends Component {
    constructor(props) {
        super(props)

		this.state={
			name:this.props.location.state.employeeData.name,
			id: this.props.match.params.employeeId,
			dateOfBirth:new Date(this.props.location.state.employeeData.dateOfBirth),
			departments:[],
			department:{
				id:this.props.location.state.employeeData.departmentId
				},
			mobile:this.props.location.state.employeeData.mobile,
			gender:this.props.location.state.employeeData.gender,
			errors: {}
		}
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleSelectChange=this.handleSelectChange.bind(this);
		this.handleDateChange=this.handleDateChange.bind(this);
    }
	
    componentDidMount(){
		DepartmentService.getAll().then((res) => {
            this.setState({ departments: res.data});
        }).catch(error=> {
		  console.log(error);
		});
    }
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleSelectChange=(event)=>{		
		this.setState({ department: {id:event.target.value} })
	}
	
	handleDateChange=(date)=>{		
		this.setState({dateOfBirth: date})
	}
	
	handleValidation = () => {
		let name = this.state.name;
		let mobile = this.state.mobile;
		let gender = this.state.gender;
		let department = this.state.department.id;
		let errors = {};
		let formIsValid = true;

		if (!name) {
		  formIsValid = false;
		  errors["name"] = "Name Cannot be empty";
		}
		
		if (!mobile) {
		  formIsValid = false;
		  errors["mobile"] = "Mobile Cannot be empty";
		}
		
		if (!gender) {
		  formIsValid = false;
		  errors["gender"] = "Gender Cannot be empty";
		}
		
		if (!department) {
		  formIsValid = false;
		  errors["department"] = "Department Cannot be empty";
		}

		this.setState({ errors: errors });
		return formIsValid;
	}
	
	handleSubmit=(e)=>{
		e.preventDefault();
		console.log(this.state.department);

		if (this.handleValidation()) {
			var postData={
			name:this.state.name,
			departmentId:this.state.department.id,
			mobile:this.state.mobile,
			gender:this.state.gender,
			dateOfBirth:this.state.dateOfBirth
			}

			EmployeeService.update(postData, this.state.id)
			.then(response => {
				toast("Employee updated successfully",{autoClose:2000, position: toast.POSITION.TOP_CENTER}) 
			})
			.catch(error=> {
			  console.log(error);
			})
		  
		} 
	}

    render() {
		const title=<h3>Update Employee</h3>
		const data = this.state.departments;
		let name =data.map((department) => {
			if(department.active==1)
			return <option value={department.id} key={department.id}>{department.name}</option>
		})
		
        return (
            <div>
                <Container>
					{title}					
					<Form className="dept-form">
					  <FormGroup row>
						<Label for="exampleEmail">Name</Label>
						  <Input type="text" name="name" id="name" value={this.state.name} className="form-control  my-1" onChange={this.handleChange} placeholder="" />
						<p style={{ color: "red" }}>{this.state.errors["name"]}</p>

						<Label for="exampleEmail" id="department" >Department</Label>
							<select className="form-control  my-1" value={this.state.department.id} onChange={this.handleSelectChange}>
						  <option value="">Select Department</option>
							{name}
						  </select>
						  <p style={{ color: "red" }}>{this.state.errors["department"]}</p>
			
						<Label for="exampleEmail">Mobile</Label>
							<Input type="text" name="mobile" className="form-control  my-1" id="mobile" value={this.state.mobile} onChange={this.handleChange} placeholder="" />
						<p style={{ color: "red" }}>{this.state.errors["mobile"]}</p>
			
						<Label for="exampleSelect">Date</Label>
						  <DatePicker 
						  className="form-control  my-1"
						  name="dateOfBirth" value={this.state.dateOfBirth}
						  selected={this.state.dateOfBirth} 
						  onChange={this.handleDateChange}
						  dateFormat="MM/dd/yyyy"
						  />
			
						<Label for="exampleEmail">Gender</Label>
							<select className="form-control  my-1" name="gender" value={this.state.gender} onChange={this.handleChange}>
							  <option value="">Select Gender</option>
							  <option value="MALE">Male</option>
							  <option value="FEMALE">Female</option>
							  <option value="OTHERS">Others</option>
							</select>
							<p style={{ color: "red" }}>{this.state.errors["gender"]}</p>
			
						  <Button onClick={this.handleSubmit}>Submit</Button>
					  </FormGroup>
					</Form>
				</Container>
            </div>
        );
    }
}

export default UpdateEmployee