import React, { Component } from 'react';
import {Route, BrowserRouter as Router, Switch} from 'react-router-dom';
import DepartmentList from  './DepartmentList';
import EmployeeList from  './EmployeeList';
import UpdateDepartment from  './UpdateDepartment';
import UpdateEmployee from  './UpdateEmployee';
import EmployeesByDepartment from  './EmployeesByDepartment';

class MyRoute extends Component {
    render() {
        return (
            <div>
				<Router>
					<Switch>
					<Route exact path="/" component={DepartmentList} />
					<Route exact path="/department" component={DepartmentList} />
					<Route exact path="/employee" component={EmployeeList} />
					<Route path = "/update-department/:departmentId" component = {UpdateDepartment}></Route>
					<Route path = "/update-employee/:employeeId" component = {UpdateEmployee}></Route>
					<Route path = "/departments/:departmentId/employees" component = {EmployeesByDepartment}></Route>
					</Switch>
				</Router>
            </div>
        );
    }
}

export default MyRoute;