import React, { Component } from 'react'
import { Link } from "react-router-dom";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import DepartmentService from '../services/DepartmentService'
import EmployeeService from '../services/EmployeeService'
import { Container, Col, Button, Form, FormGroup, Label, Input, Table } from 'reactstrap'
import '../custom.css'
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import moment from "moment"

toast.configure()

class EmployeeList extends Component {
    constructor() {
        super()
		this.state={
			dateOfBirth:new Date(),
			employees:[],
			departments:[],
			name:"",
			department:{id:'', name:''},
			mobile:"",
			gender:"",
			errors: {},
			isLoading: false
		}
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleSelectChange=this.handleSelectChange.bind(this);
		this.handleDateChange=this.handleDateChange.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
    }

    componentDidMount(){
        DepartmentService.getAll().then((res) => {
            this.setState({ departments: res.data});
        }).catch(error=> {
		  console.log(error);
		});
		
		EmployeeService.getAll().then((res) => {
            this.setState({ employees: res.data});
        }).catch(error=> {
		  console.log(error);
		});
    }
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleSelectChange=(event)=>{		
		this.setState({ department: {id:event.target.value} })
	}
	
	handleDateChange=(date)=>{		
		this.setState({dateOfBirth: date})
	}
	
	handleDelete=(id)=>{	
		EmployeeService.delete(id).then( res => {
            this.setState({employees: this.state.employees.filter(employee => employee.id !== id)});
			toast("Employee deleted successfully",{autoClose:2000, position: toast.POSITION.TOP_CENTER})  
        });
	}
	
	handleValidation = () => {
		let name = this.state.name;
		let mobile = this.state.mobile;
		let gender = this.state.gender;
		let department = this.state.department.id;
		let errors = {};
		let formIsValid = true;

		if (!name) {
		  formIsValid = false;
		  errors["name"] = "Name Cannot be empty";
		}
		
		if (!mobile) {
		  formIsValid = false;
		  errors["mobile"] = "Mobile Cannot be empty";
		}
		
		if (!gender) {
		  formIsValid = false;
		  errors["gender"] = "Gender Cannot be empty";
		}
		
		if (!department) {
		  formIsValid = false;
		  errors["department"] = "Department Cannot be empty";
		}

		this.setState({ errors: errors });
		return formIsValid;
	}
	
	handleSubmit=(e)=>{
		
		e.preventDefault();
		
		this.setState({ isLoading: true });

		if (this.handleValidation()) {
			var postData={
				name:this.state.name,
				departmentId:this.state.department.id,
				mobile:this.state.mobile,
				gender:this.state.gender,
				dateOfBirth:this.state.dateOfBirth
			}
			EmployeeService.create(postData)
			.then(response => {
				this.setState(prevState => {
					   return {
						   employees: [...prevState.employees, response.data],
						   isLoading: false
					   }
				});
				
				toast("Employee added successfully",{autoClose:2000, position: toast.POSITION.TOP_CENTER}) 

			})
			.catch(error=> {
			  console.log(error);
			})	
		  
		} else {
		  
		}
	}
	
    render() {
		const title=<h2>Add Employee</h2>
		const data = this.state.departments;
		let name =data.map((department) => {
			if(department.active==1)
			return <option value={department.id} key={department.id}>{department.name}</option>
		})
		
		const employees = this.state.employees;
		let rows =employees.map((employee, index) => {
			 	return	(<tr>
						  <td>{++index}</td>
						  <td>{employee.name}</td>
						  <td>{employee.code}</td>
						  <td>{moment(employee.dateOfBirth).format("yyyy-MM-DD")}</td>
						  <td>{employee.gender}</td>
						  <td>{employee.mobile}</td>
						  <td>
						  {
							  data.map((department) => {
								if(employee.departmentId==department.id)
								return <h5>{department.name}</h5>
							})
						  }
						  </td>
						  <td>
							<Link className="btn btn-info btn-sm" to={{
							  pathname: "/update-employee/" + employee.id,
							  state: {
								employeeData: employee
							  }
							}}>Edit</Link>&nbsp;&nbsp;
							<Button className="btn btn-danger btn-sm" onClick={()=>this.handleDelete(employee.id)}>Delete</Button>
						  </td>
						</tr>)
		})
		
        return (
            <div>
                <Container>
					{title}
					<Form className="dept-form">
					  <FormGroup row>
						<Label for="exampleEmail">Name</Label>
						  <Input type="text" name="name" id="name" className="form-control  my-1" onChange={this.handleChange} placeholder="" />
						<p style={{ color: "red" }}>{this.state.errors["name"]}</p>

						<Label for="exampleEmail" id="department" >Department</Label>
							<select className="form-control  my-1"  onChange={this.handleSelectChange}>
						  <option value="">Select Department</option>
							{name}
						  </select>
						  <p style={{ color: "red" }}>{this.state.errors["department"]}</p>
			
						<Label for="exampleEmail">Mobile</Label>
							<Input type="text" name="mobile" className="form-control  my-1" id="mobile" onChange={this.handleChange} placeholder="" />
						<p style={{ color: "red" }}>{this.state.errors["mobile"]}</p>
			
						<Label for="exampleSelect">Date</Label>
						  <DatePicker 
						  className="form-control  my-1"
						  name="dateOfBirth" 
						  selected={this.state.dateOfBirth} 
						  onChange={this.handleDateChange}
						  dateFormat="MM/dd/yyyy"
						  />
			
						<Label for="exampleEmail">Gender</Label>
							<select className="form-control  my-1" name="gender" onChange={this.handleChange}>
							  <option value="">Select Gender</option>
							  <option value="MALE">Male</option>
							  <option value="FEMALE">Female</option>
							  <option value="OTHERS">Others</option>
							</select>
							<p style={{ color: "red" }}>{this.state.errors["gender"]}</p>
			
						  <Button onClick={this.handleSubmit}>Submit</Button>
					  </FormGroup>
					</Form>
				</Container>
				<hr/>
				<Container>
					<Table bordered>
					  <thead>
						<tr>
						  <th>S/N</th>
						  <th>Name</th>
						  <th>Code</th>
						  <th>Date Of Birth</th>
						  <th>Gender</th>
						  <th>Mobile</th>
						  <th>Department</th>
						  <th>Action</th>
						</tr>
					  </thead>
					  <tbody>
							{rows}
					  </tbody>
					</Table>
				</Container>
            </div>
        );
    }
}

export default EmployeeList