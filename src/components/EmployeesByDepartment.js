import React, { Component } from 'react'
import EmployeeService from '../services/EmployeeService'
import { Container, Table } from 'reactstrap'

class EmployeesByDepartment extends Component {
    constructor(props) {
        super(props)
		this.state={
			id: this.props.match.params.departmentId,
			department:this.props.location.state.departmentData.name,
			employees:[]
		}
    }

    componentDidMount(){
		EmployeeService.getByDepartmentId(this.state.id).then((response) => {
			this.setState({ employees: response.data});
            
        }).catch(error=> {
		  console.log(error);
		});
    }
	
    render() {
		const title=<h2>Add Employee</h2>
		const employees = this.state.employees;
		let rows =employees.map((employee, index) => {
			 	return	(<tr>
						  <td>{++index}</td>
						  <td>{employee.name}</td>
						  <td>{employee.code}</td>
						  <td>{employee.dateOfBirth}</td>
						  <td>{employee.gender}</td>
						  <td>{employee.mobile}</td>
						</tr>)
		})
		
        return (
            <div>
				<Container>
				   <p>
				   <h5><b>{this.state.department} department employees</b></h5>
				   </p>
				   <hr/>
					<Table bordered>
					  <thead>
						<tr>
						  <th>S/N</th>
						  <th>Name</th>
						  <th>Code</th>
						  <th>Date Of Birth</th>
						  <th>Gender</th>
						  <th>Mobile</th>
						</tr>
					  </thead>
					  <tbody>
							{rows}
					  </tbody>
					</Table>
				</Container>
            </div>
        );
    }
}

export default EmployeesByDepartment