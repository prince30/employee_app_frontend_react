import React, { Component } from 'react'
import { Link } from "react-router-dom";
import DepartmentService from '../services/DepartmentService'
import { Container, Col, Button, Form, FormGroup, Label, Input, Table } from 'reactstrap'
import '../custom.css'
import {toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

toast.configure() 

class DepartmentList extends Component {
    constructor(props) {
        super(props)

        this.state = {
                departments: [],
				name:"",
				active:false,
				errors: {},
				isLoading: false
        }
		
		this.handleSubmit=this.handleSubmit.bind(this);
		this.handleChange=this.handleChange.bind(this);
		this.handleChecked=this.handleChecked.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
    }
	
	handleValidation = () => {
		let name = this.state.name;
		let errors = {};
		let formIsValid = true;

		if (!name) {
		  formIsValid = false;
		  errors["name"] = "Name Cannot be empty";
		}

		this.setState({ errors: errors });
		return formIsValid;
	}

    componentDidMount(){
        DepartmentService.getAll().then((res) => {
            this.setState({ departments: res.data});
        }).catch(error=> {
		  console.log(error);
		});
    }
	
	handleChange=(event)=>{		
		this.setState({[event.target.name] : event.target.value})
	}
	
	handleChecked = () => {
		this.setState(initialState => ({
		  active: !initialState.active,
		}));
	  }
	
	handleDelete=(id)=>{	
		DepartmentService.delete(id).then( res => {
            this.setState({departments: this.state.departments.filter(department => department.id !== id)});
			toast("Department deleted successfully",{autoClose:2000, position: toast.POSITION.TOP_CENTER})  
        });
	}
	
	handleSubmit=(e)=>{
		e.preventDefault();
		
		this.setState({ isLoading: true });

		if (this.handleValidation()) {
				var postData={
				name:this.state.name,
				active:this.state.active
			}

			DepartmentService.create(postData)
			.then(response => {
				this.setState(prevState => {
					   return {
						   departments: [...prevState.departments, response.data],
						   isLoading: false
					   }
				});
				toast("Department added successfully",{autoClose:2000, position: toast.POSITION.TOP_CENTER})  
			})
			.catch(error=> {
			  console.log(error);
			})
		  
		} else {
		  
		}
	}
	
    render() {
		const title=<h3>Add Department</h3>
		let departmentList = this.state.departments;
		const rows = departmentList.map((department, index) => {
			 	return	(<tr>
						  <td>{++index}</td>
						  <td>{department.name}</td>
						  <td>{department.active==1 ? 'Active' : 'InActive'}</td>
						  <td>
							<Link className="btn btn-success btn-sm"  
							to={{
							  pathname: "/departments/" + department.id +"/employees",
							  state: {
								departmentData: department
							  }
							}}>View</Link>
						  </td>
						  <td>
							<Link className="btn btn-info btn-sm" to = {"/update-department/" + department.id}>Edit</Link>&nbsp;&nbsp;
							<Button className="btn btn-danger btn-sm" onClick={()=>this.handleDelete(department.id)}>Delete</Button>&nbsp;&nbsp;
						  </td>
						</tr>)
		})
		
        return (
            <div>
                <Container>
					{title}
					<Form className="dept-form">
						<FormGroup row>
							<Label for="exampleEmail">Name</Label>
							<Col sm={4}>
								<Input type="text" name="name" className="form-control" id="name" onChange={this.handleChange} placeholder="" />
								<p style={{ color: "red" }}>{this.state.errors["name"]}</p>
							</Col>
							<Col sm={6} className="check-box">
								<Label check>
									<Input type="checkbox" name="active" id="active" checked={this.state.active} onChange={this.handleChecked} />
									Active
								</Label>
								<Button className="btn-sm btn-submit" onClick={this.handleSubmit}>Submit</Button>
							</Col>
						</FormGroup>
					</Form>
				</Container>
				<hr/>
				<Container>
					<Table bordered>
					  <thead>
						<tr>
						  <th>S/N</th>
						  <th>Name</th>
						  <th>Status</th>
						  <th>Employees</th>
						  <th>Action</th>
						</tr>
					  </thead>
					  <tbody>
							{rows}
					  </tbody>
					</Table>
				</Container>
            </div>
        );
    }
}

export default DepartmentList