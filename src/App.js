import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from  './components/AppNavbar';
import MyRoute from  './components/MyRoute';

function App() {
  return (
    <div className = "App">
		<AppNavbar/>
    </div>
  );
}

export default App;
